/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.natchamon.usermanagement;

/**
 *
 * @author ADMIN
 */
public class TestUserService {
    public static void main(String[] args) {
        System.out.println(UserService.getUsers());
        UserService.addUser(new User("userx", "password"));
        System.out.println(UserService.getUsers());
        User updateUser = new User("usery", "password"); 
        UserService.updateUser(3, updateUser);
        System.out.println(UserService.getUsers());
        UserService.delUser(updateUser);
        System.out.println(UserService.getUsers());
        UserService.delUser(1);
        System.out.println(UserService.getUsers());
    }
}