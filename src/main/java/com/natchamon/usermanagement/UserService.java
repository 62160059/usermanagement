/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.natchamon.usermanagement;

import java.util.ArrayList;

/**
 *
 * @author ADMIN
 */
public class UserService {
    private static ArrayList<User> userList = new ArrayList<>();;
    static{
        
        //Mock Data
        userList.add(new User("admin", "password"));
        userList.add(new User("user1", "password"));
        userList.add(new User("user2", "password"));
    }
    //create (C)
    public static boolean addUser(User user){
        userList.add(user);
        return true;
    }
    // Delete (D)
    public static boolean delUser(User user){
        userList.remove(user);
        return true;
    }
    public static boolean delUser(int index){
        userList.remove(index);
        return true;
    }
    //read (R)
    public static ArrayList<User> getUsers(){
        return userList;
    }  
    public static User getUser(int index){
        return userList.get(index);
    }
    // Update (U)
    public static boolean updateUser(int index, User user){
        userList.set(index, user);
        return true;
    }
    // file
    public static void save(){
    }
    public static void load(){
        
    }
    
    public static User authenticate(String userName, String password){
        for(int i=0; i< userList.size(); i++){
            User user = userList.get(i);
            if(user.getUserName().equals(userName) && user.getPassword().equals(password)){
                return  user;
            }
        }
        return null;
    }
}